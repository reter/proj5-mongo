
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

import flask
import arrow
import acp_times  #
import config
import logging




# Globals
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb





# New routes -IN DEVELOPMENT-
@app.route('/submit', methods=['POST'])
def submit():
    data = request.form

    miles = []
    km = []
    location = []
    open_times = []
    close_times = []



    size = 0
    i = 0

    for key in data.keys():
        for value in data.getlist(key):
            if value != '':
                if key == 'miles':
                    miles.append(value)
                    size += 1
                elif key == 'km' :
                    km.append(value)
                elif key == 'open' :
                    open_times.append(value)
                elif key == 'close' :
                    close_times.append(value)
                elif key == 'distance':
                    dist = value


    for value in data.getlist('location'):
        location.append(value)
        i += 1
        if i == size:
            break

    db.tododb.remove({})
    for x in range(size):
        item_doc = {
           'distance': dist,
           'miles': miles[x],
           'km': km[x],
           'location': location[x],
           'open_time': open_times[x],
           'close_time': close_times[x]
        }
        db.tododb.insert_one(item_doc)
        dist = ""
    return render_template('calc.html')


# Routes from app.py
@app.route('/done')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)

# Routes from flask_brevets.py
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', -1, type=float)
    begin_time = request.args.get('begin_time')
    begin_date = request.args.get('begin_date')
    distance = request.args.get('distance')
    app.logger.debug("request.args: {}".format(request.args))

    if km == -1:
        result = {"error": 2}
        return flask.jsonify(result=result)

    distance = float(distance)
    if km > distance * 1.2:
        result = {"error": 1}
        return flask.jsonify(result=result)

    time = begin_date + " " + begin_time
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)

    result = {"open": open_time, "close": close_time, "error": 0}
    return flask.jsonify(result=result)


#############
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
